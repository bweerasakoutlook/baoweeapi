﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;

namespace baoweeapi.Areas.Identity.Data;

// Add profile data for application users by adding properties to the User class
public class User : IdentityUser
{
    [Required(ErrorMessage ="full name can not empty")]
    public string FullName { get; set; } = null!;
    public string Photo { get; set; } = "nopic.png";
}

