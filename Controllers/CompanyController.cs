﻿using Microsoft.AspNetCore.Mvc;

namespace baoweeapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompanyController : ControllerBase
    {
        [Route("")] // api/Company/
        [HttpGet]
        public IActionResult Index()
        {
            return Ok(new { messgae = "Hello Company" });
        }

        [Route("about")] //api/Company/about
        [HttpGet]
        public IActionResult About()
        {
            return Ok(new { messgae = "Hello about us" });
        }

        [Route("product/{id}/name/{productname}")] //api/Company/product/5/name/coke
        [HttpGet]
        public IActionResult GetProductById(int id, string productname) => Ok(new { messgae = $"Product : {id} ProductName : {productname}" });

        [Route("search")] //api/Company/search?name=NT&age=100
        [HttpGet]
        public IActionResult Search([FromQuery] string name, [FromQuery] int age)
        {
            return Ok(new { messgae = $"Search {name} {age}" });
        }

        [Route("search2/{id}/filter")] //api/Company/search2/10/filter?name=NT&age=100
        [HttpGet]
        public IActionResult Search2(int id, [FromQuery] string name, [FromQuery] int age)
        {
            return Ok(new { messgae = $"Search {id} {name} {age}" });
        }

        //api/Company/search2/10/filter?name=NT&age=100
        [HttpGet("search3/{id}/filter")]
        public IActionResult Search3(int id, [FromQuery] string name, [FromQuery] int age)
        {
            return Ok(new { messgae = $"Search {id} {name} {age}" });
        }
    }
}
