﻿
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using baoweeapi.Data;
using baoweeapi.Models;
using Microsoft.AspNetCore.Authorization;

namespace baoweeapi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly APIContext _context;

        public ProductController(APIContext context)
        {
            _context = context;
        }

        // GET: api/Product
        //[Authorize(Roles = "Admin")]
        [Authorize(Roles = "admin")]
        [HttpGet]
        public async Task<IActionResult> GetProduct()
        {
            if (_context.Product == null)
            {
                return NotFound();
            }

            //var product = await _context.Product
            //    .Include(p=>p.Category)
            //    .ToListAsync();

            var product = await _context.Product
                .Include(p => p.Category)
                .Select(p => new
                {
                    p.ProductId,
                    p.ProductName,
                    p.CategoryId,
                    p.Category!.CategoryName
                })
                .ToListAsync();
            
            return Ok(product);
        }

        // GET: api/Product/5
        [Authorize(Roles = "manager")]
        [HttpGet("{id}")]
        public async Task<ActionResult<Product>> GetProduct(int id)
        {
            if (_context.Product == null)
            {
                return NotFound();
            }

            var product = await _context.Product
                .Where(p=>p.ProductId==id)
                .Include(_p => _p.Category)
                .SingleOrDefaultAsync();

            if (product == null)
            {
                return NotFound();
            }

            return product;
        }

        // PUT: api/Product/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProduct(int id, Product product)
        {
            if (id != product.ProductId)
            {
                return BadRequest();
            }

            _context.Entry(product).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Product
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Product>> PostProduct(Product product)
        {
            if (_context.Product == null)
            {
                return Problem("Entity set 'APIContext.Product'  is null.");
            }
            await _context.Product.AddAsync(product);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetProduct", new { id = product.ProductId }, product);
        }

        // DELETE: api/Product/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProduct(int id)
        {
            if (_context.Product == null)
            {
                return NotFound();
            }
            var product = await _context.Product.FindAsync(id);
            if (product == null)
            {
                return NotFound();
            }

            _context.Product.Remove(product);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool ProductExists(int id)
        {
            return (_context.Product?.Any(e => e.ProductId == id)).GetValueOrDefault();
        }

        //search from product name
        //api/product/search?ProductName=c
        [HttpGet("search")]
        public async Task<IActionResult> SearchProduct([FromQuery]string ProductName)
        {
            var resault = await _context.Product
                .Where(p => EF.Functions.Like(p.ProductName,$"%{ProductName}%"))
                .Include(p => p.Category)
                .ToListAsync();

            //if (resault == null)
            //{
            //    return NotFound();
            //}
            return Ok(resault); 
        }
    }
}
