﻿using baoweeapi.Services.ThaiDate;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace baoweeapi.Controllers
{
    //localhost:7121/api/home
    [Route("api/[controller]")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        private readonly IThaiDate _thaiDate;
        public HomeController(IThaiDate thaiDate) // inject
        {
            _thaiDate = thaiDate;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var response = new
            {
                api = "NT api running ....",
                Date = $"วันนี้วันที่ {_thaiDate.ShowThaiDate()}"
            };
            return Ok(response);
        }

    }
}
