﻿
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using baoweeapi.Data;
using baoweeapi.Models;

namespace baoweeapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly APIContext _context;

        public CategoryController(APIContext context)
        {
            _context = context;
        }

        //// GET: api/Category
        //[HttpGet]
        //public async Task<IActionResult> GetCategory()
        //{
        //  if (_context.Category == null)
        //  {
        //      return NotFound();
        //  }
        //    //var category = await _context.Category
        //    //    .Select(a=>new { a.CategoryId,a.CategoryName })
        //    //    .OrderByDescending(a=>a.CategoryId)
        //    //    .AsNoTracking()
        //    //    .ToListAsync(); //AsNoTracking() >> faster query

        //    //sql
        //    var category = await _context.Category.FromSqlRaw("select * from category order by categoryname desc").ToListAsync();
        //    var total = await _context.Category.CountAsync();
        //    return Ok(new 
        //    {
        //        //category,
        //        data = category,
        //        //total,
        //        totalRecord = total
        //    });
        //}

        // GET: api/Category?page=1&pageSize=3
        [HttpGet]
        public async Task<IActionResult> GetCategory(int page = 1,int pageSize=3)
        {
            if (_context.Category == null)
            {
                return NotFound();
            }

            var category = await _context.Category
                .OrderByDescending(c=> c.CategoryId)
                .Skip((page-1)*pageSize)
                .Take(pageSize)
                .AsNoTracking()
                .ToListAsync();
            var total = await _context.Category.CountAsync();
            return Ok(new
            {
                //category,
                data = category,
                //total,
                totalRecord = total
            });
        }

        // GET: api/Category/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Category>> GetCategory(int id)
        {
          if (_context.Category == null)
          {
              return NotFound(new {message = "empty category"});
          }
            var category = await _context.Category.FindAsync(id);

            if (category == null)
            {
                return NotFound(new {message = "can not found category matched your id "});
            }

            return category;
        }

        // PUT: api/Category/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCategory(int id, Category category)
        {
            if (id != category.CategoryId)
            {
                return BadRequest(new {message = $"your id {id} not matched"});
            }

            _context.Entry(category).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CategoryExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Category
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Category>> PostCategory(Category category)
        {
          if (_context.Category == null)
          {
              return Problem("Entity set 'APIContext.Category'  is null.");
          }
            await _context.Category.AddAsync(category);
            await _context.SaveChangesAsync();

            //return CreatedAtAction("GetCategory", new { id = category.CategoryId }, category);
            return CreatedAtAction("GetCategory",new {id = category.CategoryId}, new {message = "add new category successfull", data = category});
        }

        // DELETE: api/Category/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCategory(int id)
        {
            if (_context.Category == null)
            {
                return NotFound();
            }
            var category = await _context.Category.FindAsync(id);
            if (category == null)
            {
                return NotFound();
            }

            _context.Category.Remove(category);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool CategoryExists(int id)
        {
            return (_context.Category?.Any(e => e.CategoryId == id)).GetValueOrDefault();
        }
    }
}
