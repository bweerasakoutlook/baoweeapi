﻿using baoweeapi.Areas.Identity.Data;
using baoweeapi.DtoModels;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace AspnetNT.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public AuthController(
            UserManager<User> userManager, 
            SignInManager<User> signInManager, 
            IConfiguration configuration,
            IWebHostEnvironment webHostEnvironment
            )
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _configuration = configuration;
            _webHostEnvironment = webHostEnvironment;
        }

        // api/auth/register
        [HttpPost]
        [RequestSizeLimit(10*1024*1024)] //approx. 10 Mb
        public async Task<IActionResult> Register(RegisterDto registerDto)
        {
            // เช็คอีเมล์ซ้ำ
            var userEmail = await _userManager.FindByEmailAsync(registerDto.Email);
            if (userEmail != null) return Conflict(new { message = "email already exist" });

            var user = new User
            {
                FullName = registerDto.FullName,
                Email = registerDto.Email,
                UserName = registerDto.Email
            };

            //upload photo
            if (registerDto.Photo != "nopic.png")
            {
                var base64array = Convert.FromBase64String(registerDto.Photo!);
                var newFilename = Guid.NewGuid().ToString() + ".png";

                // path for upload
                var uploadPath = Path.Combine($"{_webHostEnvironment.WebRootPath}/upload/{newFilename}");

                //upload file to path
                await System.IO.File.WriteAllBytesAsync(uploadPath, base64array);
                user.Photo = newFilename;

            }

            var result = await _userManager.CreateAsync(user, registerDto.Password);
            if (!result.Succeeded)
            {
                return BadRequest(new { message = "เกิดข้อผิดพลาด ลงทะเบียนไม่สำเร็จ", resultMessage = result.Errors });
            }

            return Created("", new { message = "ลงทะเบียนสำเร็จ" });
        }

        // api/auth/login
        [HttpPost]
        public async Task<IActionResult> Login(LoginDto loginDto)
        {
            var user = await _userManager.FindByEmailAsync(loginDto.Email);
            if (user == null) return NotFound(new { message = "ไม่พบผู้ใช้นี้ไม่ในระบบ" });

            var result = await _signInManager.PasswordSignInAsync(loginDto.Email, loginDto.Password, false, false);
            if (!result.Succeeded)
            {
                return Unauthorized(new { message = "รหัสผ่านไม่ถูกต้อง" });
            }

            return await CreateToken(user.Email);
        }

        private async Task<IActionResult> CreateToken(string email)
        {
            var user = await _userManager.FindByEmailAsync(email);
            if (user == null) return Unauthorized();

            // เตรียมข้อมูลสำหรับสร้าง token
            var JWT_KEY = Encoding.UTF8.GetBytes(_configuration.GetSection("JWT_KEY").Value); //get secret key from appsettings.json

            var payload = new List<Claim>
            {
                new Claim("user_id", user.Id.ToString()),
                new Claim(JwtRegisteredClaimNames.Email, user.Email)
            };

            // เพิ่ม role เข้าไปใน payload data กรณีที่ใช้ role-base authorization
            var roles = await _userManager.GetRolesAsync(user);
            payload.AddRange(roles.Select(role => new Claim(ClaimsIdentity.DefaultRoleClaimType, role)));

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(payload),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(JWT_KEY), SecurityAlgorithms.HmacSha256Signature),
                Expires = DateTime.UtcNow.AddDays(7)
            };

            // สร้าง token
            var tokenHandler = new JwtSecurityTokenHandler();
            var token = tokenHandler.CreateToken(tokenDescriptor);

            var resultToken = new
            {
                access_token = tokenHandler.WriteToken(token),
                expiration = token.ValidTo
            };

            return Ok(resultToken);
        }

        // get user profile
        // api/auth/profile
        [Authorize]
        [HttpGet]
        public async Task<IActionResult> Profile()
        {
            ////get token from http header
            //var token = await HttpContext.GetTokenAsync("access_token");

            ////decode token and get user id
            //var payload = new JwtSecurityToken(token);
            //var user_id = payload.Claims.First(p => p.Type=="user_id").Value; //user id from token

            //get user id from token
            var user_id = User.Claims.First(p => p.Type == "user_id").Value;

            //get user profile from database
            var userProfile = await _userManager.FindByIdAsync(user_id);

            return Ok(new
            {
                Id =userProfile.Id,
                Fullname = userProfile.FullName,
                Email = userProfile.Email,
                PhotoUrl = $"{Request.Scheme}://{Request.Host}/upload/{userProfile.Photo}",
            });

            
        }

        //update profile
        // api/auth/updateprofile
        [Authorize]
        [HttpPost]
        public async Task<IActionResult> UpdateProfile(UpdateProfileDto profileDto)
        {
            var user_id = User.Claims.First(p => p.Type == "user_id").Value;
            var user = await _userManager.FindByIdAsync(user_id);
            if (user == null) return NotFound();
            user.FullName = profileDto.FullName;
            var result = await _userManager.UpdateAsync(user);

            if (!result.Succeeded)
            {
                return BadRequest(new { message = "error can not update" });
            }

            return Ok(new { message = "update data succeed" });

                    
        }
    }
}
