﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace baoweeapi.Models
{
    public class Product
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProductId { get; set; }
        //[Column(TypeName ="ntext")]
        [Required(ErrorMessage = "product name can not empty")]
        public string ProductName { get; set; } = null!;
        [Required(ErrorMessage = "product price can not empty")]
        [Range(0.00,9999999.99,ErrorMessage ="price invalid")]
        [Column(TypeName ="decimal(10,2)")]
        public decimal ProductPrice { get; set; }
        [Column(TypeName ="DateTime")]
        public DateTime ProductExpire { get; set; } = DateTime.Now.AddMonths(3);

        //FK
        [ForeignKey("cat_id")]
        [Required(ErrorMessage ="category id can not empty")]
        public int CategoryId { get; set; } 

        //relation
        //manu to one

        public Category? Category { get; set; }

    }
}
