﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace baoweeapi.Models
{
    //[Table("tbl_types")]
    public class Category
    {
        //[Column("cat_id")]
        //[Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CategoryId { get; set; }
        //[Column(TypeName ="ntext")]
        [Required(ErrorMessage = "product category can not empty")]
        public string CategoryName { get; set; } = null!;
        public bool IsActive { get; set; } = true;
    }
}
