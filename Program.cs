using baoweeapi.Services.ThaiDate;
using Microsoft.EntityFrameworkCore;
using baoweeapi.Data;
using Microsoft.AspNetCore.Identity;
using baoweeapi.Areas.Identity.Data;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;

var builder = WebApplication.CreateBuilder(args);

//override port for production
builder.WebHost.UseUrls("http://0.0.0.0:8000");

// add CORS
builder.Services.AddCors();

//custom kestrel web option
builder.WebHost.ConfigureKestrel(serverOptions=>
{
    serverOptions.Limits.MaxRequestBodySize = 10 * 1024 * 1024; //limit 100 MB
    //serverOptions.Limits.MaxConcurrentConnections = 10;
    serverOptions.Limits.RequestHeadersTimeout = TimeSpan.FromMinutes(2); // 2minutes
});


var connectionString = builder.Configuration.GetConnectionString("IdentityContext");

builder.Services.AddDbContext<IdentityContext>(options =>
    options.UseSqlServer(connectionString));

builder.Services.AddDefaultIdentity<User>(options => options.SignIn.RequireConfirmedAccount = false)
    .AddRoles<IdentityRole>()
    .AddEntityFrameworkStores<IdentityContext>();

//custom identity options
builder.Services.Configure<IdentityOptions>(options =>
{
    options.Password.RequiredLength = 6;
    options.Password.RequireNonAlphanumeric = false;
    options.Password.RequireDigit = false;
    options.Password.RequireLowercase = false;
    options.Password.RequireUppercase = false;
});


//add jwt service for validate token
builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(option =>
{
    option.TokenValidationParameters = new TokenValidationParameters()
    {
        ValidateIssuerSigningKey = true,
        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(builder.Configuration.GetSection("JWT_KEY").Value)),
        ValidateIssuer = false,
        ValidateAudience = false,
        ClockSkew = TimeSpan.Zero,
    };

});


builder.Services.AddDbContext<APIContext>(options =>

    options.UseSqlServer(builder.Configuration.GetConnectionString("APIContext") ?? throw new InvalidOperationException("Connection string 'APIContext' not found.")));

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

//BindingAddress my custom service
builder.Services.AddScoped<IThaiDate, ThaiDate>();

var app = builder.Build();

//global cors policy
app.UseCors(option => {
    //option.WithOrigins("Domain Name");
    option.AllowAnyOrigin();
    option.AllowAnyMethod();
    option.AllowAnyHeader();
});

//enable static file (www.root)
app.UseStaticFiles();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();
app.UseAuthentication();;

app.UseAuthorization();

app.MapControllers();

app.Run();
