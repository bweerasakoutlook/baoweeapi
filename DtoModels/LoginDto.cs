﻿using System.ComponentModel.DataAnnotations;

namespace baoweeapi.DtoModels
{
    public class LoginDto
    {
        [Required(ErrorMessage = "Email can not empty")]
        [EmailAddress(ErrorMessage = "Invalid email format")]
        public string Email { get; set; } = null!;
        [Required(ErrorMessage = "Password can not empty")]
        [StringLength(100, ErrorMessage = "password lenght must more {2} character and not over {1} character", MinimumLength = 6)]
        public string Password { get; set; } = null!;
    }
}
