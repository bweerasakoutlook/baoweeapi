﻿using Microsoft.EntityFrameworkCore;
using baoweeapi.Models;

namespace baoweeapi.Data
{
    public class APIContext : DbContext
    {
        public APIContext (DbContextOptions<APIContext> options)
            : base(options)
        {
        }

        public DbSet<Category> Category { get; set; } = null!;
        public DbSet<Product> Product { get; set; } = null!;
    }
}
